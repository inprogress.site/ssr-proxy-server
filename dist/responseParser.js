"use strict";

var _httpStatusCodes = require("http-status-codes");

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (res, cb) {
  var _writeHead = res.writeHead;
  var _write = res.write;
  var _end = res.end;
  var body = new Buffer("");

  res.writeHead = function () {};

  res.write = function (data) {
    body = Buffer.concat([body, data]);
  };

  res.end = function (data, encoding) {
    var json = null;

    if (data) {
      body = Buffer.concat([body, data]);
    }

    if (res.statusCode === _httpStatusCodes2.default.OK) {
      try {
        json = JSON.parse(body.toString(encoding));
      } catch (err) {
        cb(err); // eslint-disable-line
        res.statusCode = _httpStatusCodes2.default.BAD_GATEWAY;
        res.statusMessage = "Bad Gateway";
      }

      if (json) {
        cb(null, json); // eslint-disable-line
      }
    }

    _writeHead.call(res, res.statusCode, res.statusMessage, res.headers);
    _write.call(res, body, encoding);
    _end.call(res);
  };
};