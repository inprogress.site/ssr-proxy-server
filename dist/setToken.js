"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (proxyReq, req, res) {
  if (req.cookies && req.cookies["token"]) {
    proxyReq.setHeader("Authorization", "Bearer " + req.cookies["token"]);
  }
};