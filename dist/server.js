"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _cookieParser = require("cookie-parser");

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _compression = require("compression");

var _compression2 = _interopRequireDefault(_compression);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _ssr2 = require("./ssr");

var _ssr3 = _interopRequireDefault(_ssr2);

var _cookieHandler = require("./cookieHandler");

var _cookieHandler2 = _interopRequireDefault(_cookieHandler);

var _setToken = require("./setToken");

var _setToken2 = _interopRequireDefault(_setToken);

var _httpProxyMiddleware = require("http-proxy-middleware");

var _httpProxyMiddleware2 = _interopRequireDefault(_httpProxyMiddleware);

var _url = require("url");

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Server = function () {
  function Server() {
    _classCallCheck(this, Server);

    this.app = (0, _express2.default)();
    this.app.set("port", process.env.PORT);
    this.app.disable("x-powered-by");
    this.app.use((0, _compression2.default)());
    this.app.use((0, _cookieParser2.default)());
  }

  _createClass(Server, [{
    key: "port",
    value: function port(number) {
      this.app.set("port", number);
      return this;
    }
  }, {
    key: "api",
    value: function api(path, options) {
      var cookies = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var urls = arguments[3];

      var opts = options;
      if (cookies) {
        opts = Object.assign(options, {
          onProxyReq: _setToken2.default,
          onProxyRes: (0, _cookieHandler2.default)(urls)
        });
      }
      this.app.use(path, (0, _httpProxyMiddleware2.default)(opts));
      return this;
    }
  }, {
    key: "publicFolder",
    value: function publicFolder(folder) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var folderPath = _path2.default.join(process.cwd(), folder);

      this.app.use(_express2.default.static(folderPath, { index: index }));
      return this;
    }
  }, {
    key: "ssr",
    value: function ssr(options) {
      this.app.use((0, _ssr3.default)(options));
      return this;
    }
  }, {
    key: "start",
    value: function start() {
      var _this = this;

      this.app.listen(this.app.get("port"), function () {
        console.log("Express server starting on port: " + _this.app.get("port"));
      });
    }
  }]);

  return Server;
}();

exports.default = Server;