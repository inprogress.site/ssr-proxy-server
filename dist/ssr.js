"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

require("ignore-styles");

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require("react-router");

var _reactRouterDom = require("react-router-dom");

var _server = require("react-dom/server");

var _reactRedux = require("react-redux");

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _template = require("./template");

var _template2 = _interopRequireDefault(_template);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _react2.default.createFactory(_reactRouter.StaticRouter);
var provider = _react2.default.createFactory(_reactRedux.Provider);

exports.default = function (options) {
  var assetManifest = JSON.parse(_fs2.default.readFileSync(options.assetManifest, "UTF-8"));

  return function (request, response) {
    var context = {};

    var render = function render() {
      // Grab the initial state from our Redux store
      var initialState = JSON.stringify(options.store.getState());

      var html = (0, _server.renderToString)(_react2.default.createElement(
        _reactRedux.Provider,
        { store: options.store },
        _react2.default.createElement(
          _reactRouter.StaticRouter,
          { location: request.originalUrl, context: context },
          _react2.default.createElement(options.App, null)
        )
      ));

      var status = 200;
      if (context.url) {
        if (context.statusCode) {
          status = context.statusCode;
        }
      }

      response.status(status).send((0, _template2.default)({
        root: html,
        cssBundle: process.env.NODE_ENV === "development" ? "" : "/" + assetManifest["main.css"],
        jsBundle: process.env.NODE_ENV === "development" ? options.hmr + "/js/bundle.js" : "/" + assetManifest["main.js"],
        initialState: initialState
      }));
    };

    var promises = [];
    options.routes.some(function (route) {
      var match = (0, _reactRouterDom.matchPath)(request.url, route);
      if (match && route.fetchData) {
        promises.push(route.fetchData(options.store.dispatch, match, {
          baseUrl: options.baseUrl
        }));
      }
      return match;
    });

    if (!promises.length) {
      render();
    } else {
      Promise.all(promises).then(function () {
        render();
      });
    }
  };
};