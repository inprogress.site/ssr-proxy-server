'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactHelmet = require('react-helmet');

var _reactHelmet2 = _interopRequireDefault(_reactHelmet);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (options) {
  var helmet = _reactHelmet2.default.renderStatic();
  return '\n    <!DOCTYPE html>\n    <html ' + helmet.htmlAttributes.toString() + ' >\n      <head>\n        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />\n        <meta charset="utf-8">\n        <meta name="viewport" content="width=device-width, initial-scale=1">\n        <link rel="shortcut icon" href="/favicon.ico">\n        <link rel="manifest" href="/manifest.json">\n        ' + helmet.title.toString() + '\n        ' + helmet.meta.toString() + '\n        ' + helmet.link.toString() + '\n        ' + helmet.script.toString() + '\n        ' + (options.cssBundle ? '<link rel="stylesheet" type="text/css" href="' + options.cssBundle + '">' : "") + '\n      </head>\n      <body ' + helmet.bodyAttributes.toString() + '>\n        <div id="root"><div>' + options.root + '</div></div>\n        <script>\n          window.__PRELOADED_STATE__ = ' + options.initialState + '\n        </script>\n        <script src="' + options.jsBundle + '"></script>\n        <script>\n          /*if (\'serviceWorker\' in navigator) {\n            window.addEventListener(\'load\', function() {\n              navigator.serviceWorker.register(\'/service-worker.js\');\n            });\n        }*/\n        </script>\n      </body>\n    </html>\n  ';
};