"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _httpStatusCodes = require("http-status-codes");

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

var _responseParser = require("./responseParser");

var _responseParser2 = _interopRequireDefault(_responseParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (urls) {
  return function (proxyRes, req, res) {
    if (req.url === urls.login && proxyRes.statusCode === _httpStatusCodes2.default.OK) {
      (0, _responseParser2.default)(res, function (err, body) {
        if (err) {
          return console.error("setTokenCookie::responseParser: ", err);
        }

        if (body && body.access_token) {
          res.cookie("token", body.access_token, { httpOnly: true });
        }
      });
    }

    if (req.url === urls.logout && proxyRes.statusCode === _httpStatusCodes2.default.OK) {
      res.clearCookie("token", { httpOnly: true });
    }
  };
};