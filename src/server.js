// @flow

import express from "express";
import type { $Application } from "express";
import cookieParser from "cookie-parser";
import compression from "compression";
import path from "path";
import ssr from "./ssr";
import type { SsrOptions } from "./ssr";
import cookieHandler from "./cookieHandler";
import type { urlsParam } from "./cookieHandler";
import setToken from "./setToken";
import proxy from "http-proxy-middleware";
import url from "url";

export default class Server {
  app: $Application;

  constructor() {
    this.app = express();
    this.app.set("port", process.env.PORT);
    this.app.disable("x-powered-by");
    this.app.use(compression());
    this.app.use(cookieParser());
  }

  port(number: number) {
    this.app.set("port", number);
    return this;
  }

  api(
    path: string,
    options: Object,
    cookies: boolean = false,
    urls: urlsParam
  ) {
    let opts = options;
    if (cookies) {
      opts = Object.assign(options, {
        onProxyReq: setToken,
        onProxyRes: cookieHandler(urls)
      });
    }
    this.app.use(path, proxy(opts));
    return this;
  }

  publicFolder(folder: string, index: boolean = false) {
    const folderPath = path.join(process.cwd(), folder);

    this.app.use(express.static(folderPath, { index }));
    return this;
  }

  ssr(options: SsrOptions) {
    this.app.use(ssr(options));
    return this;
  }

  start() {
    this.app.listen(this.app.get("port"), () => {
      console.log(`Express server starting on port: ${this.app.get("port")}`);
    });
  }
}
