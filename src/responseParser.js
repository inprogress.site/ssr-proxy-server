import httpStatusCodes from "http-status-codes";

module.exports = (res, cb) => {
  const _writeHead = res.writeHead;
  const _write = res.write;
  const _end = res.end;
  let body = new Buffer("");

  res.writeHead = () => {};

  res.write = data => {
    body = Buffer.concat([body, data]);
  };

  res.end = (data, encoding) => {
    let json = null;

    if (data) {
      body = Buffer.concat([body, data]);
    }

    if (res.statusCode === httpStatusCodes.OK) {
      try {
        json = JSON.parse(body.toString(encoding));
      } catch (err) {
        cb(err); // eslint-disable-line
        res.statusCode = httpStatusCodes.BAD_GATEWAY;
        res.statusMessage = "Bad Gateway";
      }

      if (json) {
        cb(null, json); // eslint-disable-line
      }
    }

    _writeHead.call(res, res.statusCode, res.statusMessage, res.headers);
    _write.call(res, body, encoding);
    _end.call(res);
  };
};
