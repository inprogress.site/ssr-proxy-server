// @flow
import type { $Request as Request, $Response as Response } from "express";

export default (proxyReq, req: Request, res: Response) => {
  if (req.cookies && req.cookies["token"]) {
    proxyReq.setHeader("Authorization", `Bearer ${req.cookies["token"]}`);
  }
};
