// @flow

import httpStatusCodes from "http-status-codes";
import responseParser from "./responseParser";
import type { $Request as Request, $Response as Response } from "express";

export interface urlsParam {
  login: string;
  logout: string;
}

interface LoginResponse {
  token_type: string;
  expires_in: number;
  access_token: string;
  refresh_token: string;
}

export default (urls: urlsParam) => {
  return (proxyRes, req: Request, res: Response) => {
    if (req.url === urls.login && proxyRes.statusCode === httpStatusCodes.OK) {
      responseParser(res, (err, body: LoginResponse) => {
        if (err) {
          return console.error("setTokenCookie::responseParser: ", err);
        }

        if (body && body.access_token) {
          res.cookie("token", body.access_token, { httpOnly: true });
        }
      });
    }

    if (req.url === urls.logout && proxyRes.statusCode === httpStatusCodes.OK) {
      res.clearCookie("token", { httpOnly: true });
    }
  };
};
