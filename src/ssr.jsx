// @flow

import "ignore-styles";

import React from "react";
import type { Node } from "react";
import { StaticRouter as Router } from "react-router";
import { matchPath } from "react-router-dom";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import type { Dispatch, Store } from "redux";
import fs from "fs";
import { default as template, TemplateOptions } from "./template";

const router = React.createFactory(Router);
const provider = React.createFactory(Provider);

export interface SsrOptions {
  assetManifest: string;
  hmr: string;
  App: Node;
  routes: Array<Object>;
  store: { getState: Function, dispatch: Function };
  baseUrl: string;
}

export default (options: SsrOptions) => {
  const assetManifest: { [id: string]: string } = JSON.parse(
    fs.readFileSync(options.assetManifest, "UTF-8")
  );

  return (request: Object, response: Object) => {
    let context: { url?: string, statusCode?: number } = {};

    const render = () => {
      // Grab the initial state from our Redux store
      const initialState = JSON.stringify(options.store.getState());

      const html = renderToString(
        <Provider store={options.store}>
          <Router location={request.originalUrl} context={context}>
            <options.App />
          </Router>
        </Provider>
      );

      let status: number = 200;
      if (context.url) {
        if (context.statusCode) {
          status = context.statusCode;
        }
      }

      response.status(status).send(
        template({
          root: html,
          cssBundle:
            process.env.NODE_ENV === "development"
              ? ""
              : `/${assetManifest["main.css"]}`,
          jsBundle:
            process.env.NODE_ENV === "development"
              ? `${options.hmr}/js/bundle.js`
              : `/${assetManifest["main.js"]}`,
          initialState
        })
      );
    };

    let promises = [];
    options.routes.some(
      (route: {
        fetchData?: (
          dispatch: Dispatch,
          match,
          options: { baseUrl: string }
        ) => void
      }) => {
        const match = matchPath(request.url, route);
        if (match && route.fetchData) {
          promises.push(
            route.fetchData(options.store.dispatch, match, {
              baseUrl: options.baseUrl
            })
          );
        }
        return match;
      }
    );

    if (!promises.length) {
      render();
    } else {
      Promise.all(promises).then(() => {
        render();
      });
    }
  };
};
