// @flow

import Helmet from "react-helmet";

export interface TemplateOptions {
  cssBundle?: string;
  root: string;
  initialState: string;
  jsBundle: string;
}

export default (options: TemplateOptions) => {
  const helmet = Helmet.renderStatic();
  return `
    <!DOCTYPE html>
    <html ${helmet.htmlAttributes.toString()} >
      <head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="manifest" href="/manifest.json">
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${helmet.script.toString()}
        ${options.cssBundle
          ? '<link rel="stylesheet" type="text/css" href="' +
            options.cssBundle +
            '">'
          : ""}
      </head>
      <body ${helmet.bodyAttributes.toString()}>
        <div id="root"><div>${options.root}</div></div>
        <script>
          window.__PRELOADED_STATE__ = ${options.initialState}
        </script>
        <script src="${options.jsBundle}"></script>
        <script>
          /*if ('serviceWorker' in navigator) {
            window.addEventListener('load', function() {
              navigator.serviceWorker.register('/service-worker.js');
            });
        }*/
        </script>
      </body>
    </html>
  `;
};
